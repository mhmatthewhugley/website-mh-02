#
# Made by Matthew
# Steam: https://steamcommunity.com/id/MatthewGameing/
# Discord: Matthew#8358
#
# How to use
# 1. Spawn e2
# 2. Spawn a screen with two values first one Distance second one Mode
# 3. Wire A from screen to e2 Screen
# 4. Wire B from screen to e2 Mode
# 5. Hold c and right click the e2 chip and click debug
#
# Also DBPOM stands for Distance Between Player Or Model

@name DBPOM
@outputs Screen Mode
@persist G:entity Mode:number

# These are chat commands below

if(Mode == 1) {
runOnTick(1)
if(tickClk()) {
    T = owner():lastSaid():explode(" ")
    if(T[1,string] == "!cm") {
        Mode = 0
        print("Changed to model")
    }
}
}

if(Mode == 0) {
runOnTick(1)
if(tickClk()) {
    T = owner():lastSaid():explode(" ")
    if(T[1,string] == "!cp") {
        Mode = 1
        print("Changed to player")
    }
}
}

if(Mode == 0) {
runOnTick(1)
if(tickClk()) {
    T = owner():lastSaid():explode(" ")
    if(T[1,string] == "!model") {
        T:remove(1)
        findByModel(T:concat(" "))
    }
}
}

if(Mode == 1) {
runOnTick(1)
if(tickClk()) {
    T = owner():lastSaid():explode(" ")
    if(T[1,string] == "!player") {
        T:remove(1)
        findByName(T:concat(" "))
    }
}
}

# This is the distance below

findSortByDistance(entity():pos())
G = find()

Mi = toUnit("mi",G:pos():distance(owner():pos()))
Ft = toUnit("ft",G:pos():distance(owner():pos()))

if(Mi >= 1) {
Screen = round(Mi,0)
}
else {
Screen = round(Ft,0)
}
