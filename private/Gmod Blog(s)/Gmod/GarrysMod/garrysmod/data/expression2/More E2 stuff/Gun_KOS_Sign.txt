@name Gun KOS Sign
@inputs EGP:wirelink
@persist N:number A:number

if(first())
{
    interval(200)
    
    #entity():createWire(entity():isWeldedTo(), "EGP", "wirelink")
    EGP:egpClear()
    
    EGP:egpBox(1, vec2(192, 384), vec2(128, 64))
    EGP:egpMaterial(1, "vgui/gfx/vgui/awp")
    
    EGP:egpBox(2, vec2(258, 363), vec2(32, 16))
    EGP:egpMaterial(2, "vgui/gfx/vgui/bullet")
    EGP:egpAngle(2, 15)
    
    EGP:egpRoundedBox(3, vec2(256, 280), vec2(256, 52))
    EGP:egpColor(3, vec(35))
    
    EGP:egpRoundedBox(4, vec2(256, 280), vec2(250, 46))
    EGP:egpColor(4, vec(112, 25, 25))
    
    EGP:egpText(5, "KOS INSIDE", vec2(256, 280))
    EGP:egpAlign(5, 1, 1)
    EGP:egpSize(5, 42)
}

else
{
    N += 9
    A += 1
    if(A > 20) {A = 0}
    
    EGP:egpAngle(1, -10 + abs(cos(90 + N)) * 35)
    
    EGP:egpPos(2, vec2(258 + A, 367 - A))
    interval(100)
}
