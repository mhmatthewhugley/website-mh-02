--Made by Matthew#8358
local ScreenGui = Instance.new("ScreenGui")
local maingui = Instance.new("Frame")
local Pinewoodcomputercoretp = Instance.new("TextButton")
local Printposition = Instance.new("TextButton")
local Prisonlifechangeteams = Instance.new("TextButton")
local Prisonlifeitems = Instance.new("TextButton")
local closemaingui = Instance.new("TextButton")
local Madeby = Instance.new("TextLabel")
local opengui = Instance.new("Frame")
local Openmaingui = Instance.new("TextButton")
local tpscriptsgui = Instance.new("Frame")
local security = Instance.new("TextButton")
local tier4 = Instance.new("TextButton")
local fans = Instance.new("TextButton")
local fans2 = Instance.new("TextButton")
local coolant = Instance.new("TextButton")
local closetpscriptsgui = Instance.new("TextButton")
local prisonlifeitemsgui = Instance.new("Frame")
local Remington870 = Instance.new("TextButton")
local AK47 = Instance.new("TextButton")
local CrudeKnife = Instance.new("TextButton")
local Hammer = Instance.new("TextButton")
local M9 = Instance.new("TextButton")
local closeprisonlifeitemsgui = Instance.new("TextButton")
local prisonlifeteamgui = Instance.new("Frame")
local Inmates = Instance.new("TextButton")
local Guards = Instance.new("TextButton")
local Neutral = Instance.new("TextButton")
local closeprisonlifeteamgui = Instance.new("TextButton")

ScreenGui.Parent = game.CoreGui

maingui.Name = "maingui"
maingui.Parent = ScreenGui
maingui.BackgroundColor3 = Color3.new(1, 1, 1)
maingui.BorderSizePixel = 5
maingui.Position = UDim2.new(0.403037131, 0, 0.324382961, 0)
maingui.Size = UDim2.new(0, 197, 0, 259)
maingui.Visible = false

Pinewoodcomputercoretp.Name = "Pinewood computer core tp"
Pinewoodcomputercoretp.Parent = maingui
Pinewoodcomputercoretp.BackgroundColor3 = Color3.new(1, 1, 1)
Pinewoodcomputercoretp.BorderSizePixel = 3
Pinewoodcomputercoretp.Position = UDim2.new(0.00507614203, 0, 0.308880299, 0)
Pinewoodcomputercoretp.Size = UDim2.new(0, 196, 0, 44)
Pinewoodcomputercoretp.Font = Enum.Font.SourceSans
Pinewoodcomputercoretp.Text = "Pinewood Computer Core tp scrips"
Pinewoodcomputercoretp.TextColor3 = Color3.new(0, 0, 0)
Pinewoodcomputercoretp.TextScaled = true
Pinewoodcomputercoretp.TextSize = 14
Pinewoodcomputercoretp.TextWrapped = true
Pinewoodcomputercoretp.MouseButton1Down:connect(function()
	tpscriptsgui.Visible = true
end)

Printposition.Name = "Print position"
Printposition.Parent = maingui
Printposition.BackgroundColor3 = Color3.new(1, 1, 1)
Printposition.BorderSizePixel = 3
Printposition.Position = UDim2.new(0.00507614203, 0, 0.828700542, 0)
Printposition.Size = UDim2.new(0, 196, 0, 44)
Printposition.Font = Enum.Font.SourceSans
Printposition.Text = "Execute print current position in F9"
Printposition.TextColor3 = Color3.new(0, 0, 0)
Printposition.TextScaled = true
Printposition.TextSize = 14
Printposition.TextWrapped = true
Printposition.MouseButton1Down:connect(function()
print(game.Players.LocalPlayer.Character.HumanoidRootPart.Position)
end)

Prisonlifechangeteams.Name = "Prison life change teams"
Prisonlifechangeteams.Parent = maingui
Prisonlifechangeteams.BackgroundColor3 = Color3.new(1, 1, 1)
Prisonlifechangeteams.BorderSizePixel = 3
Prisonlifechangeteams.Position = UDim2.new(0.00507614203, 0, 0.654955328, 0)
Prisonlifechangeteams.Size = UDim2.new(0, 196, 0, 44)
Prisonlifechangeteams.Font = Enum.Font.SourceSans
Prisonlifechangeteams.Text = "Prison life change teams"
Prisonlifechangeteams.TextColor3 = Color3.new(0, 0, 0)
Prisonlifechangeteams.TextScaled = true
Prisonlifechangeteams.TextSize = 14
Prisonlifechangeteams.TextWrapped = true
Prisonlifechangeteams.MouseButton1Down:connect(function()
	prisonlifeteamgui.Visible = true
end)

Prisonlifeitems.Name = "Prison life items"
Prisonlifeitems.Parent = maingui
Prisonlifeitems.BackgroundColor3 = Color3.new(1, 1, 1)
Prisonlifeitems.BorderSizePixel = 3
Prisonlifeitems.Position = UDim2.new(0.00507614203, 0, 0.481210172, 0)
Prisonlifeitems.Size = UDim2.new(0, 196, 0, 44)
Prisonlifeitems.Font = Enum.Font.SourceSans
Prisonlifeitems.Text = "Prison life items"
Prisonlifeitems.TextColor3 = Color3.new(0, 0, 0)
Prisonlifeitems.TextScaled = true
Prisonlifeitems.TextSize = 14
Prisonlifeitems.TextWrapped = true
Prisonlifeitems.MouseButton1Down:connect(function()
	prisonlifeitemsgui.Visible = true
end)

closemaingui.Name = "close maingui"
closemaingui.Parent = maingui
closemaingui.BackgroundColor3 = Color3.new(1, 1, 1)
closemaingui.BorderSizePixel = 3
closemaingui.Position = UDim2.new(0.898339748, 0, -0.000429034233, 0)
closemaingui.Size = UDim2.new(0, 20, 0, 20)
closemaingui.ZIndex = 2
closemaingui.Font = Enum.Font.SourceSans
closemaingui.Text = "X"
closemaingui.TextColor3 = Color3.new(0, 0, 0)
closemaingui.TextSize = 25
closemaingui.TextWrapped = true
closemaingui.MouseButton1Down:connect(function()
	maingui.Visible = false
end)

Madeby.Name = "Made by"
Madeby.Parent = maingui
Madeby.BackgroundColor3 = Color3.new(1, 1, 1)
Madeby.BorderSizePixel = 3
Madeby.Position = UDim2.new(0.00507614203, 0, 0, 0)
Madeby.Size = UDim2.new(0, 196, 0, 80)
Madeby.Font = Enum.Font.SourceSans
Madeby.Text = "Made by Matthew#8358"
Madeby.TextColor3 = Color3.new(0, 0, 0)
Madeby.TextScaled = true
Madeby.TextSize = 14
Madeby.TextWrapped = true

opengui.Name = "opengui"
opengui.Parent = ScreenGui
opengui.BackgroundColor3 = Color3.new(1, 1, 1)
opengui.BorderSizePixel = 3
opengui.Position = UDim2.new(-0.00111853331, 0, 0.724755645, 0)
opengui.Size = UDim2.new(0, 48, 0, 30)

Openmaingui.Name = "Open maingui"
Openmaingui.Parent = opengui
Openmaingui.BackgroundColor3 = Color3.new(1, 1, 1)
Openmaingui.Position = UDim2.new(0.020833334, 0, 0, 0)
Openmaingui.Size = UDim2.new(0, 47, 0, 30)
Openmaingui.Font = Enum.Font.SourceSans
Openmaingui.Text = "Open"
Openmaingui.TextColor3 = Color3.new(0, 0, 0)
Openmaingui.TextScaled = true
Openmaingui.TextSize = 14
Openmaingui.TextWrapped = true
Openmaingui.MouseButton1Down:connect(function()
	maingui.Visible = true
end)

tpscriptsgui.Name = "tpscriptsgui"
tpscriptsgui.Parent = ScreenGui
tpscriptsgui.BackgroundColor3 = Color3.new(1, 1, 1)
tpscriptsgui.BorderSizePixel = 5
tpscriptsgui.Position = UDim2.new(0.565160036, 0, 0.177604899, 0)
tpscriptsgui.Size = UDim2.new(0, 169, 0, 153)
tpscriptsgui.Visible = false

security.Name = "security"
security.Parent = tpscriptsgui
security.BackgroundColor3 = Color3.new(1, 1, 1)
security.BorderSizePixel = 3
security.Position = UDim2.new(0.512000024, 0, 0.345999986, 0)
security.Size = UDim2.new(0, 82, 0, 50)
security.Font = Enum.Font.SourceSans
security.Text = "Security"
security.TextColor3 = Color3.new(0, 0, 0)
security.TextScaled = true
security.TextSize = 14
security.TextWrapped = true
security.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( 72, 707, -125))
end)

tier4.Name = "tier4"
tier4.Parent = tpscriptsgui
tier4.BackgroundColor3 = Color3.new(1, 1, 1)
tier4.BorderSizePixel = 3
tier4.Position = UDim2.new(0.513070107, 0, 0.672555566, 0)
tier4.Size = UDim2.new(0, 82, 0, 50)
tier4.Font = Enum.Font.SourceSans
tier4.Text = "Tier 4"
tier4.TextColor3 = Color3.new(0, 0, 0)
tier4.TextScaled = true
tier4.TextSize = 14
tier4.TextWrapped = true
tier4.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( 72, 707, -120))
end)

fans.Name = "fans"
fans.Parent = tpscriptsgui
fans.BackgroundColor3 = Color3.new(1, 1, 1)
fans.BorderSizePixel = 3
fans.Position = UDim2.new(-0.00138306618, 0, -0.00930187199, 0)
fans.Size = UDim2.new(0, 82, 0, 50)
fans.Font = Enum.Font.SourceSans
fans.Text = "Fans"
fans.TextColor3 = Color3.new(0, 0, 0)
fans.TextScaled = true
fans.TextSize = 14
fans.TextWrapped = true
fans.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( -485, 718, -286))
end)

fans2.Name = "fans2"
fans2.Parent = tpscriptsgui
fans2.BackgroundColor3 = Color3.new(1, 1, 1)
fans2.BorderSizePixel = 3
fans2.Position = UDim2.new(-0.00172275305, 0, 0.34575808, 0)
fans2.Size = UDim2.new(0, 82, 0, 50)
fans2.Font = Enum.Font.SourceSans
fans2.Text = "Fans2"
fans2.TextColor3 = Color3.new(0, 0, 0)
fans2.TextScaled = true
fans2.TextSize = 14
fans2.TextWrapped = true
fans2.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( -480, 718, -286))
end)

coolant.Name = "coolant"
coolant.Parent = tpscriptsgui
coolant.BackgroundColor3 = Color3.new(1, 1, 1)
coolant.BorderSizePixel = 3
coolant.Position = UDim2.new(0, 0, 0.672999978, 0)
coolant.Size = UDim2.new(0, 82, 0, 50)
coolant.Font = Enum.Font.SourceSans
coolant.Text = "Coolant"
coolant.TextColor3 = Color3.new(0, 0, 0)
coolant.TextScaled = true
coolant.TextSize = 14
coolant.TextWrapped = true
coolant.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(Vector3.new( 173, 534, -951))
end)

closetpscriptsgui.Name = "close tpscriptsgui"
closetpscriptsgui.Parent = tpscriptsgui
closetpscriptsgui.BackgroundColor3 = Color3.new(1, 1, 1)
closetpscriptsgui.BorderSizePixel = 3
closetpscriptsgui.Position = UDim2.new(0.51238668, 0, -0.00527822971, 0)
closetpscriptsgui.Size = UDim2.new(0, 82, 0, 50)
closetpscriptsgui.ZIndex = 2
closetpscriptsgui.Font = Enum.Font.SourceSans
closetpscriptsgui.Text = "X"
closetpscriptsgui.TextColor3 = Color3.new(0, 0, 0)
closetpscriptsgui.TextSize = 80
closetpscriptsgui.TextWrapped = true
closetpscriptsgui.MouseButton1Down:connect(function()
	tpscriptsgui.Visible = false
end)

prisonlifeitemsgui.Name = "prisonlifeitemsgui"
prisonlifeitemsgui.Parent = ScreenGui
prisonlifeitemsgui.BackgroundColor3 = Color3.new(1, 1, 1)
prisonlifeitemsgui.BorderSizePixel = 5
prisonlifeitemsgui.Position = UDim2.new(0.565287173, 0, 0.395822614, 0)
prisonlifeitemsgui.Size = UDim2.new(0, 169, 0, 153)
prisonlifeitemsgui.Visible = false

Remington870.Name = "Remington 870"
Remington870.Parent = prisonlifeitemsgui
Remington870.BackgroundColor3 = Color3.new(1, 1, 1)
Remington870.BorderSizePixel = 3
Remington870.Position = UDim2.new(0.513070166, 0, 0.332686275, 0)
Remington870.Size = UDim2.new(0, 82, 0, 50)
Remington870.Font = Enum.Font.SourceSans
Remington870.Text = "Remington 870"
Remington870.TextColor3 = Color3.new(0, 0, 0)
Remington870.TextScaled = true
Remington870.TextSize = 14
Remington870.TextWrapped = true
Remington870.MouseButton1Down:connect(function()
local tbl_main = 
{
      game:GetService("Workspace")["Prison_ITEMS"].giver["Remington 870"].ITEMPICKUP
}
game:GetService("Workspace").Remote.ItemHandler:InvokeServer(unpack(tbl_main))
end)

AK47.Name = "AK-47"
AK47.Parent = prisonlifeitemsgui
AK47.BackgroundColor3 = Color3.new(1, 1, 1)
AK47.BorderSizePixel = 3
AK47.Position = UDim2.new(0.513070107, 0, 0.672555566, 0)
AK47.Size = UDim2.new(0, 82, 0, 50)
AK47.Font = Enum.Font.SourceSans
AK47.Text = "AK-47"
AK47.TextColor3 = Color3.new(0, 0, 0)
AK47.TextScaled = true
AK47.TextSize = 14
AK47.TextWrapped = true
AK47.MouseButton1Down:connect(function()
local tbl_main = 
{
      game:GetService("Workspace")["Prison_ITEMS"].giver["AK-47"].ITEMPICKUP
}
game:GetService("Workspace").Remote.ItemHandler:InvokeServer(unpack(tbl_main))
end)

CrudeKnife.Name = "Crude Knife"
CrudeKnife.Parent = prisonlifeitemsgui
CrudeKnife.BackgroundColor3 = Color3.new(1, 1, 1)
CrudeKnife.BorderSizePixel = 3
CrudeKnife.Position = UDim2.new(-0.00138306618, 0, -0.0027659242, 0)
CrudeKnife.Size = UDim2.new(0, 82, 0, 50)
CrudeKnife.Font = Enum.Font.SourceSans
CrudeKnife.Text = "Crude Knife"
CrudeKnife.TextColor3 = Color3.new(0, 0, 0)
CrudeKnife.TextScaled = true
CrudeKnife.TextSize = 14
CrudeKnife.TextWrapped = true
CrudeKnife.MouseButton1Down:connect(function()
local tbl_main = 
{
      game:GetService("Workspace")["Prison_ITEMS"].single["Crude Knife"].ITEMPICKUP
}
game:GetService("Workspace").Remote.ItemHandler:InvokeServer(unpack(tbl_main))
end)

Hammer.Name = "Hammer"
Hammer.Parent = prisonlifeitemsgui
Hammer.BackgroundColor3 = Color3.new(1, 1, 1)
Hammer.BorderSizePixel = 3
Hammer.Position = UDim2.new(-0.00172275305, 0, 0.34575808, 0)
Hammer.Size = UDim2.new(0, 82, 0, 50)
Hammer.Font = Enum.Font.SourceSans
Hammer.Text = "Hammer"
Hammer.TextColor3 = Color3.new(0, 0, 0)
Hammer.TextScaled = true
Hammer.TextSize = 14
Hammer.TextWrapped = true
Hammer.MouseButton1Down:connect(function()
local tbl_main = 
{
      game:GetService("Workspace")["Prison_ITEMS"].single.Hammer.ITEMPICKUP
}
game:GetService("Workspace").Remote.ItemHandler:InvokeServer(unpack(tbl_main))
end)

M9.Name = "M9"
M9.Parent = prisonlifeitemsgui
M9.BackgroundColor3 = Color3.new(1, 1, 1)
M9.BorderSizePixel = 3
M9.Position = UDim2.new(0, 0, 0.672999978, 0)
M9.Size = UDim2.new(0, 82, 0, 50)
M9.Font = Enum.Font.SourceSans
M9.Text = "M9"
M9.TextColor3 = Color3.new(0, 0, 0)
M9.TextScaled = true
M9.TextSize = 14
M9.TextWrapped = true
M9.MouseButton1Down:connect(function()
local tbl_main = 
{
      game:GetService("Workspace")["Prison_ITEMS"].giver.M9.ITEMPICKUP
}
game:GetService("Workspace").Remote.ItemHandler:InvokeServer(unpack(tbl_main))
end)

closeprisonlifeitemsgui.Name = "close prisonlifeitemsgui"
closeprisonlifeitemsgui.Parent = prisonlifeitemsgui
closeprisonlifeitemsgui.BackgroundColor3 = Color3.new(1, 1, 1)
closeprisonlifeitemsgui.BorderSizePixel = 3
closeprisonlifeitemsgui.Position = UDim2.new(0.51238668, 0, -0.00527822925, 0)
closeprisonlifeitemsgui.Size = UDim2.new(0, 82, 0, 50)
closeprisonlifeitemsgui.ZIndex = 2
closeprisonlifeitemsgui.Font = Enum.Font.SourceSans
closeprisonlifeitemsgui.Text = "X"
closeprisonlifeitemsgui.TextColor3 = Color3.new(0, 0, 0)
closeprisonlifeitemsgui.TextSize = 80
closeprisonlifeitemsgui.TextWrapped = true
closeprisonlifeitemsgui.MouseButton1Down:connect(function()
	prisonlifeitemsgui.Visible = false
end)

prisonlifeteamgui.Name = "prisonlifeteamgui"
prisonlifeteamgui.Parent = ScreenGui
prisonlifeteamgui.BackgroundColor3 = Color3.new(1, 1, 1)
prisonlifeteamgui.BorderSizePixel = 5
prisonlifeteamgui.Position = UDim2.new(0.565287173, 0, 0.602986336, 0)
prisonlifeteamgui.Size = UDim2.new(0, 169, 0, 153)
prisonlifeteamgui.Visible = false

Inmates.Name = "Inmates"
Inmates.Parent = prisonlifeteamgui
Inmates.BackgroundColor3 = Color3.new(1, 1, 1)
Inmates.BorderSizePixel = 3
Inmates.Position = UDim2.new(0.483484387, 0, 0.34575808, 0)
Inmates.Size = UDim2.new(0, 87, 0, 48)
Inmates.Font = Enum.Font.SourceSans
Inmates.Text = "Inmates"
Inmates.TextColor3 = Color3.new(0, 0, 0)
Inmates.TextScaled = true
Inmates.TextSize = 14
Inmates.TextWrapped = true
Inmates.MouseButton1Down:connect(function()
local tbl_main = 
{
      "Bright orange"
}
game:GetService("Workspace").Remote.TeamEvent:FireServer(unpack(tbl_main))
end)

Guards.Name = "Guards"
Guards.Parent = prisonlifeteamgui
Guards.BackgroundColor3 = Color3.new(1, 1, 1)
Guards.BorderSizePixel = 3
Guards.Position = UDim2.new(0, 0, 0.34575808, 0)
Guards.Size = UDim2.new(0, 81, 0, 48)
Guards.Font = Enum.Font.SourceSans
Guards.Text = "Guards"
Guards.TextColor3 = Color3.new(0, 0, 0)
Guards.TextScaled = true
Guards.TextSize = 14
Guards.TextWrapped = true
Guards.MouseButton1Down:connect(function()
local tbl_main = 
{
      "Bright blue"
}
game:GetService("Workspace").Remote.TeamEvent:FireServer(unpack(tbl_main))
end)

Neutral.Name = "Neutral"
Neutral.Parent = prisonlifeteamgui
Neutral.BackgroundColor3 = Color3.new(1, 1, 1)
Neutral.BorderSizePixel = 3
Neutral.Position = UDim2.new(0, 0, 0.665740669, 0)
Neutral.Size = UDim2.new(0, 169, 0, 49)
Neutral.Font = Enum.Font.SourceSans
Neutral.Text = "Neutral"
Neutral.TextColor3 = Color3.new(0, 0, 0)
Neutral.TextScaled = true
Neutral.TextSize = 14
Neutral.TextWrapped = true
Neutral.MouseButton1Down:connect(function()
local tbl_main = 
{
      "Medium stone grey"
}
game:GetService("Workspace").Remote.TeamEvent:FireServer(unpack(tbl_main))
end)

closeprisonlifeteamgui.Name = "close prisonlifeteamgui"
closeprisonlifeteamgui.Parent = prisonlifeteamgui
closeprisonlifeteamgui.BackgroundColor3 = Color3.new(1, 1, 1)
closeprisonlifeteamgui.BorderSizePixel = 3
closeprisonlifeteamgui.Size = UDim2.new(0, 169, 0, 51)
closeprisonlifeteamgui.ZIndex = 2
closeprisonlifeteamgui.Font = Enum.Font.SourceSans
closeprisonlifeteamgui.Text = "X"
closeprisonlifeteamgui.TextColor3 = Color3.new(0, 0, 0)
closeprisonlifeteamgui.TextSize = 80
closeprisonlifeteamgui.TextWrapped = true
closeprisonlifeteamgui.MouseButton1Down:connect(function()
	prisonlifeteamgui.Visible = false
end)