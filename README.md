# Website-MH-01

This is my website labeled as Website-MH-01 originally started with this website on 6-6-2020.
The earliest I started with website(s) was on 6-5-2020.

The primary Shortened URL is:
"https://tiny.cc/WMH01"

That takes you here:
[Website link/URL](https://mhmatthewhugley.gitlab.io/Website-MH-01/),
"https://mhmatthewhugley.gitlab.io/Website-MH-01/".

The current Website URL is: "https://otgt.us.eu.org"

Information on the LICENSE(s) and what is under each LICENSE is in the [Notice/Information NOTICE.txt](./public/NOTICE.txt) file.

Issue Templates [Link](./.github/ISSUE_TEMPLATE).

[Code Of Conduct](./CODE_OF_CONDUCT.md).

### Built With

- ![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white)
- ![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white)
- ![JavaScript](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)

## Wondering what files to use for your own website? If you are then here is mostly a comprehensive list until I wrute a blog about it.

| File Location                                      | Need? | Modify? | Notes                                                                                                                                                                                              |
| -------------------------------------------------- | :---: | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Website-MH-01/.gitlab-ci.yml                       | Maybe | Maybe   | Only needs to be modified if your gitlab branch is a name other then trunk like main. Or if you are not storing the website files in a folder called public. And only needed if hosting on gitlab. |
| Website-MH-01/public/404.html                      |  Yes  | Yes     | None.                                                                                                                                                                                              |
| Website-MH-01/public/403.html                      |  No   | Yes     | Might not even be in use.                                                                                                                                                                          |
| Website-MH-01/public/blogtemplate.html             | Maybe | Yes     | Only needed if you want blogs.                                                                                                                                                                     |
| Website-MH-01/public/blogyeartemplate.html         | Maybe | Yes     | Only needed if you want blogs.                                                                                                                                                                     |
| Website-MH-01/public/template.html                 |  Yes  | Yes     | The basic template for all pages.                                                                                                                                                                  |
| Website-MH-01/public/robots.txt                    |  Yes  | Yes     | Tells webpage crawlers(certain ones if specified) information like what pages to not crawl/index and a link to the sitemap.                                                                        |
| Website-MH-01/public/urllist.txt                   |  No   | Yes     | Simpler version of sitemap.xml.                                                                                                                                                                    |
| Website-MH-01/public/sitemap.xml                   |  Yes  | Yes     | None.                                                                                                                                                                                              |
| Website-MH-01/public/sitemap.xsl                   |  Yes  | No      | This is styling for the sitemap.                                                                                                                                                                   |
| Website-MH-01/public/js/default.js                 |  Yes  | No      | Unless analyitics are added.                                                                                                                                                                       |
| Website-MH-01/public/css/default.css               |  Yes  | No      | Some lines of code can be removed.                                                                                                                                                                 |
| Website-MH-01/public/\_redirects                   |  No   | Yes     | Used by netlify/cloudflare pages                                                                                                                                                                   |
| Website-MH-01/public/\_headers                     |  No   | Yes     | Used by netlify/cloudflare pages                                                                                                                                                                   |
| Website-MH-01/netlify.toml                         |  No   | Yes     | Used only by netlify.                                                                                                                                                                              |
| Website-MH-01/public/zohoverify/verifyforzoho.html |  No   | Yes     | Only used by zoho email to verify domain the contents will have to be changed according to what they give you when setting up.                                                                     |
